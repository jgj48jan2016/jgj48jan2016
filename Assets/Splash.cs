﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (PauseDelay ());
	}

	IEnumerator PauseDelay(){
		yield return new WaitForSeconds (2f);
		Application.LoadLevel ("MainMenu");
	}

	// Update is called once per frame
	void Update () {
	
	}
}
