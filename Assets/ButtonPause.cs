﻿using UnityEngine;
using System.Collections;

public class ButtonPause : MonoBehaviour {



	public void OnMouseDown(){
		
		GameObject pauseScreen = GameObject.Find ("PauseScreen");
		GameObject sb = GameObject.Find ("SoundButton");
		GameObject rb = GameObject.Find ("RestartButton");
		GameObject rsb = GameObject.Find ("ResumeButton");
	//	Animator pauseAnim = pauseScreen.GetComponent<Animator> ();
	//	pauseAnim.SetBool ("isOn",true);

		pauseScreen.GetComponent<SpriteRenderer> ().enabled = true;
		sb.GetComponent<SpriteRenderer> ().enabled = true;
		rb.GetComponent<SpriteRenderer> ().enabled = true;
		rsb.GetComponent<SpriteRenderer> ().enabled = true;

		sb.GetComponent<Collider2D> ().enabled =true;
		rb.GetComponent<Collider2D> ().enabled = true;
		rsb.GetComponent<Collider2D> ().enabled = true;

		Time.timeScale = 0;

		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<Collider2D> ().enabled = false;
	}


}
