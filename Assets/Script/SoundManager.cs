﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundManager : MonoBehaviour {
	bool isMute;
	bool _isPause;
	MenuUI gui;
	public bool isPause { get; set; }
	Sprite[] s;
	Image img;
	// Use this for initialization
	void Start () {
		img = GameObject.Find ("Sound").GetComponent<Image> ();
		s = Resources.LoadAll<Sprite> ("UI");
		gui = new MenuUI();

		if (PlayerPrefs.HasKey ("MMute")) {
				AudioListener.volume = PlayerPrefs.GetFloat("MMute");
		} else {
				AudioListener.volume = 1;
		}
		AudioSource audio = GetComponent<AudioSource> ();
		audio.Play ();

	}

	// Update is called once per frame

	void Update () {
		
		if (Input.GetKey(KeyCode.Escape)) {
			if (Application.loadedLevel == 1) {
				Application.Quit ();
			}
	}

		Sound ();
}

	IEnumerator PauseDelay(){
		yield return new WaitForSeconds (1f);
		Time.timeScale = 0f;
	}

	public void Mute(){
		isMute = !isMute;
		AudioListener.volume = isMute ? 0 : 1;
		PlayerPrefs.SetFloat ("MMute", AudioListener.volume);
	
	}

	public Sprite GetSpriteByName(string name){
		for (int i = 0; i < s.Length; i++) {
			if (s [i].name == name) {
				return s [i];
			}
		}
		return null;
	}

	public void Sound (){
		
		if (PlayerPrefs.GetFloat("MMute") == 0) {
			img.sprite = GetSpriteByName("UI_7");
		} else {
			img.sprite = GetSpriteByName("UI_6");
		}

	}
}
