﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level1 : MonoBehaviour {

    public GameObject[] pivot;
    public GameObject prefabEnemy;
    public GameObject prefabEnemy2;
    public GameObject prefabEnemy3;
    private GameObject[] goEnemy;
    public GameObject target;
	public Text scoreText;
	public GameObject anim;
	private Animator scorePanel;
	public GameObject tuts;

    [SerializePrivateVariables]
    public static bool[] isShowEnemy;
    private float[] timerStanbyEnemy;
    private int[] condition;
    private float[] timerTouch;
	bool _isPause;
	MenuUI gui;
	public int score;

	public bool isPause{ get; set; } 
	// Use this for initialization
	void Start () {
		_isPause = false;
		checkTutorial ();
		gui = new MenuUI();
        isShowEnemy = new bool[9];
        goEnemy = new GameObject[9];
        timerStanbyEnemy = new float[9];
        condition = new int[9];
        timerTouch = new float[9];
        spownEnemy();
		score = 0;

		Time.timeScale = 1;
	}

	private void checkTutorial(){
		if (!PlayerPrefs.HasKey ("tuts")) {
			PlayerPrefs.SetString ("tuts", "true");
			tuts.SetActive (true);
			tuts.GetComponent<SpriteRenderer> ().enabled = true;
			Time.timeScale = 0f;
		} else {
			tuts.SetActive (false);
		}
	}

	public void Finishing(){
		PlayerPrefs.SetInt("Score",score);
		gui.EnableScore (anim.GetComponent<Animator>());
	
	}

	public void CheckPause(){
		if (anim.GetComponent<Animator> ().GetBool ("isPaused")) {
			gui.DisablePause (anim.GetComponent<Animator> ());
			StartCoroutine (PauseDelay());


		}else if(!anim.GetComponent<Animator> ().GetBool ("isPaused")){
			gui.EnablePause (anim.GetComponent<Animator> ());
		}

		Debug.Log (anim.GetComponent<Animator> ().GetBool ("isPaused"));
	
	}
    private void spownEnemy()
    {
        int iEnemy = (int)Random.Range(0, 9);
        if (!isShowEnemy[iEnemy])
        {
            
            timerStanbyEnemy[iEnemy] = 0;
            isShowEnemy[iEnemy] = true;
            if(iEnemy == 0 || iEnemy == 3 || iEnemy == 6)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            else if (iEnemy == 1 || iEnemy == 4 || iEnemy == 7)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy2, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 270);
            }
            else if (iEnemy == 2 || iEnemy == 5 || iEnemy == 8)
            {
                goEnemy[iEnemy] = (GameObject)Instantiate(prefabEnemy3, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
                goEnemy[iEnemy].transform.localEulerAngles = new Vector3(0, 0, 180);
            }
                
            goEnemy[iEnemy].transform.localScale = new Vector3(0, 0, 1);
            goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown",true);
            //goEnemy[iEnemy].GetComponent<Animator>().SetBool("isSpown", false);
            goEnemy[iEnemy].name = "enemy_" + iEnemy;
            condition[iEnemy] = 1;
            
        }
    }



    private void moveToEgg(int idx)
    {
        if (isShowEnemy[idx] && condition[idx] != 2)
        {
            timerStanbyEnemy[idx] += Time.deltaTime;
            if (timerStanbyEnemy[idx] > 1)
            {
                float dX = target.transform.position.x - goEnemy[idx].transform.position.x;
                float dY = target.transform.position.y - goEnemy[idx].transform.position.y;

                float rad = Mathf.Atan2(dY, dX);
                float deg = rad * Mathf.Rad2Deg;

                goEnemy[idx].transform.localEulerAngles = new Vector3(0, 0, deg);

                // if (!goEnemy[idx].GetComponent<Animator>().GetBool("isFly"))
                // {
                goEnemy[idx].GetComponent<Animator>().SetBool("isFly", true);
              //  }

                if (goEnemy[idx] != null)
                {
                    goEnemy[idx].transform.position = Vector3.MoveTowards(goEnemy[idx].transform.position, target.transform.position, Time.deltaTime * 10f);
                    
                }
            }
        }
    }

    float timerShowEnemy = 0;
	IEnumerator PauseDelay(){
		yield return new WaitForSeconds (0.3f);
		Time.timeScale = 0f;
	}


	// Update is called once per frame
	void Update () {
		scoreText.text = score.ToString();
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (!isPause) { 
				new ButtonPause ().OnMouseDown ();
				isPause = true;
			} else { 
				Application.LoadLevel ("MainMenu");
			}

		}

        timerShowEnemy += Time.deltaTime;
        if(timerShowEnemy > 0.5f)
        {
            spownEnemy();
            timerShowEnemy = 0;
        }

        moveToEgg(0);
        moveToEgg(1);
        moveToEgg(2);
        moveToEgg(3);
        moveToEgg(4);
        moveToEgg(5);
        moveToEgg(6);
        moveToEgg(7);
        moveToEgg(8);


        TouchEnemy();


        RespondDestroyEnemy(0);
        RespondDestroyEnemy(1);
        RespondDestroyEnemy(2);
        RespondDestroyEnemy(3);
        RespondDestroyEnemy(4);
        RespondDestroyEnemy(5);
        RespondDestroyEnemy(6);
        RespondDestroyEnemy(7);
        RespondDestroyEnemy(8);
    }

    private void RespondDestroyEnemy(int idx)
    {
        if(condition[idx] == 2)
        {
            timerTouch[idx] += Time.deltaTime;
            if(timerTouch[idx] > 0.5f)
            {
                timerTouch[idx] = 0;
                condition[idx] = 0;
                isShowEnemy[idx] = false;
                Destroy(goEnemy[idx]);
            }
        }
    }

    private void TouchEnemy()
	{	
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch currentTouch = Input.GetTouch(i);
                if (currentTouch.phase == TouchPhase.Began)
                {
                    Vector2 v2 = new Vector2(Camera.main.ScreenToWorldPoint(currentTouch.position).x, Camera.main.ScreenToWorldPoint(currentTouch.position).y);
                    Collider2D c2d = Physics2D.OverlapPoint(v2);

                    if (c2d != null)
                    {
                        string idxEnemy = c2d.name.Substring(6, 1);
                        int idx = int.Parse(idxEnemy);
                        //isShowEnemy[idx] = false;
						score++;
                        c2d.gameObject.GetComponent<Animator>().SetBool("isDead",true);
                        //Debug.Log("name " + c2d.name+" idx "+idxEnemy);
                        condition[idx] = 2;
                        // Destroy(c2d.gameObject);
                    }
                }
            }
        }
    }
}
