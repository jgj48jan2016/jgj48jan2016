﻿using UnityEngine;
using System.Collections;

public class MenuUI : MonoBehaviour {

    public void EnableOut(Animator anim) {
        anim.SetBool("isOut", true);
    }

    public void DisableOut(Animator anim)
    {
        anim.SetBool("isOut", false);
    }

    public void EnablePause(Animator anim) {
        anim.SetBool("isPaused", true);
    }

    public void DisablePause(Animator anim)
    {
        anim.SetBool("isPaused", false);
    }

    public void CloseTutorial(Animator anim)
    {
        anim.SetBool("isClose", true);
    }

    public void EnableScore(Animator anim) {
        anim.SetBool("isFinish", true);
    }
    public void DisableScore(Animator anim){
        anim.SetBool("isFinish", false);
    }

    public void openScene(string sceneName) {
        Application.LoadLevel(sceneName);
    }
}
