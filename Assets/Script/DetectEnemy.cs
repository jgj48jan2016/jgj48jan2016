﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DetectEnemy : MonoBehaviour {
	MenuUI gui;
	public GameObject lv;
	public GameObject j;
	public Text k;
	public GameObject l;
	int score;
	// Use this for initialization
	void Start () {
		gui = new MenuUI ();
	}

	int a = 3;
    private bool isPlayEffect = false;
    private float timerEffect = 0;
	// Update is called once per frame
	void Update () {
		
       PlayEffect();
	}

    private float timerRed = 0;
    int changeColor = 0;
    private void PlayEffect()
    {
        if (isPlayEffect)
        {
            timerEffect += Time.deltaTime;
            if(timerEffect < 0.5f)
            {
                timerRed += Time.deltaTime;
                if(timerRed > 0.1f)
                {
                    changeColor++;

                    GetComponents<SpriteRenderer>()[0].color = (changeColor %2 == 0)? Color.red:Color.white;
                    timerRed = 0;
                }
                
                //GetComponentInParent<Transform>().position = new Vector3( Mathf.Sin(timerEffect * 25f) *0.1f, GetComponentInParent<Transform>().position.y, GetComponentInParent<Transform>().position.z);
            }
            else
            {
                GetComponents<SpriteRenderer>()[0].color = Color.white;
                isPlayEffect = false;
                timerEffect = 0;
            }
        }
    } 

    private void ResetPlayEffect()
    {
        isPlayEffect = true;
        timerEffect = 0;
    }
	private void Finishing(){
		if (score > PlayerPrefs.GetInt ("Score")) {
			PlayerPrefs.SetInt ("Score", score);
			l.SetActive (true);
		} else {
			l.SetActive (false);
		}
		k.text = score.ToString();
		gui.EnableScore (j.GetComponent<Animator> ());

		StartCoroutine (deadDelay());
	}

	IEnumerator deadDelay(){
		yield return new WaitForSeconds (0.5f);
		Time.timeScale = 0;

		GameObject paus = GameObject.Find ("PauseButton");
		paus.GetComponent<SpriteRenderer> ().enabled = false;
		paus.GetComponent<Collider2D> ().enabled = false;

	}



    void OnTriggerEnter2D(Collider2D col)
	{ score = lv.GetComponent<Level1> ().score;
		if (a == 0) {
			Finishing ();
		} else {
			a--;
			DestroyEnemy(col.gameObject);
			ResetPlayEffect();
		}
        
    }

    void DestroyEnemy(GameObject t)
    {
        if (t.name.Length < 6) return;
        string idxEnemy = t.name.Substring(6, 1);
        int idx = int.Parse(idxEnemy);
        Level1.isShowEnemy[idx] = false;
        Destroy(t.gameObject);
    }
}
