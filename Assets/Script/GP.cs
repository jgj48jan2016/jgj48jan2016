﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GP : MonoBehaviour {
	
	MenuUI gui = new MenuUI();
	public GameObject[] pivot;
	public GameObject prefabEnemy;
	private GameObject[] goEnemy;
	public Text scoreText;
	public GameObject target;
	private bool[] isShowEnemy;
	private float[] timerStanbyEnemy;

	public GameObject anim;
	int score;
	public GameObject tuts;
	// Use this for initialization


	void Start () {
		if (!PlayerPrefs.HasKey ("tutsDone")) {
			PlayerPrefs.SetString ("tutsDone", "true");
			tuts.SetActive (true);
		} else {
			tuts.SetActive (false);
		}

		score = 0;
		scoreText.text = score.ToString();
		isShowEnemy = new bool[9];
		goEnemy = new GameObject[9];
		timerStanbyEnemy = new float[9];

		spownEnemy();

	}

	// Update is called once per frame
	void Update () {

		if (anim. GetComponent<Animator> ().GetBool ("isPaused")) {
			//Time.timeScale = 0f;
		} else {
			Time.timeScale = 1f;
		}
		scoreText.text = score.ToString();
		timerShowEnemy += Time.deltaTime;
		if(timerShowEnemy > 0.5f)
		{
			spownEnemy();
			timerShowEnemy = 0;

		}

		moveToEgg(0);
		moveToEgg(1);
		moveToEgg(2);
		moveToEgg(3);
		moveToEgg(4);
		moveToEgg(5);
		moveToEgg(6);
		moveToEgg(7);
		moveToEgg(8);

		if (Input.touchCount > 0) {
			for (int i = 0; i < Input.touchCount; i++) {
				Touch currentTouch = Input.GetTouch (i);
				if (currentTouch.phase == TouchPhase.Began) {
					Vector2 v2 = new Vector2(Camera.main.ScreenToWorldPoint(currentTouch.position).x, Camera.main.ScreenToWorldPoint(currentTouch.position).y);
					Collider2D c2d = Physics2D.OverlapPoint(v2);

					if (c2d != null)
					{
						string idxEnemy = c2d.name.Substring(6, 1);
						int idx = int.Parse(idxEnemy);
						isShowEnemy[idx] = false;
						//score++;
						Destroy(c2d.gameObject);
					}
				}
			}


		}

	}

	private void spownEnemy() {
		int iEnemy = (int)Random.Range(0, 9);
		if (!isShowEnemy[iEnemy]) {
			timerStanbyEnemy[iEnemy] = 0;
			isShowEnemy[iEnemy] = true;
			goEnemy[iEnemy] =(GameObject) Instantiate(prefabEnemy, pivot[iEnemy].transform.position, pivot[iEnemy].transform.rotation);
			goEnemy[iEnemy].name = "enemy_" + iEnemy;
		}
	}



	private void moveToEgg(int idx)
	{
		if (isShowEnemy[idx])
		{
			timerStanbyEnemy[idx] += Time.deltaTime;
			if (timerStanbyEnemy[idx] > 1)
			{
				if(goEnemy[idx] != null)
				{
					goEnemy[idx].transform.position = Vector3.MoveTowards(goEnemy[idx].transform.position, target.transform.position, Time.deltaTime * 5f);
				}
			}
		}
	}

	float timerShowEnemy = 0;
}
