﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour {

	bool isMute;
	public Sprite onSound;
	public Sprite offSound;
	// Use this for initialization

	void Start(){
		AudioListener.pause = false;
	}
	void Update () {
		
		//on
		if(AudioListener.pause == false){
			GetComponent<SpriteRenderer> ().sprite = onSound;

		}
		//off
		else if (AudioListener.pause == true) {
			GetComponent<SpriteRenderer> ().sprite = offSound;
		}

	}

	// Update is called once per frame

	void OnMouseDown(){
		isMute = !isMute;
		AudioListener.volume = isMute ? 0 : 1;
		//off
		if (!isMute) {
			//on
			GetComponent<SpriteRenderer> ().sprite = onSound;
		} else {
			GetComponent<SpriteRenderer> ().sprite = offSound;
		}
	}
}
