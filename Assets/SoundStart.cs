﻿using UnityEngine;
using System.Collections;

public class SoundStart : MonoBehaviour {


	public Sprite onSound;
	public Sprite offSound;
	// Use this for initialization

	void Start(){
		
	}
	void Update () {

		//on
		if(AudioListener.pause == false){
			GetComponent<SpriteRenderer> ().sprite = onSound;

		}
		//off
		else if (AudioListener.pause == true) {
			GetComponent<SpriteRenderer> ().sprite = offSound;
		}

	}

	// Update is called once per frame

	void OnMouseDown(){
		//off
		if (AudioListener.pause == true) {
			//on
			AudioListener.pause = false;
			GetComponent<SpriteRenderer> ().sprite = onSound;
		}
		//on
		if(AudioListener.pause == false){
			//off
			AudioListener.pause = true;
			GetComponent<SpriteRenderer> ().sprite = offSound;
		}
	}
}
