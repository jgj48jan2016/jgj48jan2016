﻿using UnityEngine;
using System.Collections;

public class ButtonResume : MonoBehaviour {

	void OnMouseDown(){

		GameObject pauseScreen = GameObject.Find ("PauseScreen");
		GameObject sb = GameObject.Find ("SoundButton");
		GameObject rb = GameObject.Find ("RestartButton");
		GameObject rsb = GameObject.Find ("ResumeButton");
		GameObject pb = GameObject.Find ("PauseButton");
		//	Animator pauseAnim = pauseScreen.GetComponent<Animator> ();
		//	pauseAnim.SetBool ("isOn",true);

		pauseScreen.GetComponent<SpriteRenderer> ().enabled = false;
		sb.GetComponent<SpriteRenderer> ().enabled = false;
		rb.GetComponent<SpriteRenderer> ().enabled = false;
		rsb.GetComponent<SpriteRenderer> ().enabled = false;

		sb.GetComponent<Collider2D> ().enabled = true;
		rb.GetComponent<Collider2D> ().enabled = true;
		rsb.GetComponent<Collider2D> ().enabled = true;

		pb.GetComponent<SpriteRenderer> ().enabled = true;
		pb.GetComponent<Collider2D> ().enabled = true;

		Time.timeScale = 1;

	}

}
